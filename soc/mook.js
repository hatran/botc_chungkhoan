var mook = {
    sobn: function () {
        let raw = `
        Ban quản lý các KCN
        Sở Công thương
        Sở Khoa học và Công nghệ
        Sở Lao động TBXH
        Sở Nông nghiệp và PTNT
        Sở Tài nguyên và Môi trường
        Sở Thông tin và Truyền thông`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `Sở Văn hóa, Thể thao
        Sở Y tế
        Văn phòng UBND Tỉnh
        Sở Giao thông Vận tải
        Sở Ngoại vụ
        Sở Nội vụ
        Sở Tài chính
        Sở Tư pháp
        Sở Xây dựng
        Thanh tra tỉnh
        Sở Kế hoạch và Đầu tư`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `UBND Huyện Kiến Xương
                UBND Huyện Quỳnh Phụ
                UBND Huyện Hưng Hà
                UBND Huyện Tiền Hải
                UBND Huyện Đông Hưng
                UBND Huyện Vũ Thư
                UBND Huyện Thái thụy
                UBND TP Thái Bình
                Sở Tư pháp
                Sở Tài chính
                Sở Y tế
                Sở Xây dựng
                Sở Ngoại vụ
                Sở Nội vụ
                Thanh tra tỉnh`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `UBND Huyện Thái thụy
        Sở Y tế
        Văn phòng UBND Tỉnh
        Sở Giao thông Vận tải
        Sở Ngoại vụ
        Sở Nội vụ
        Sở Tài chính
        Sở Tư pháp
        Sở Xây dựng
        Thanh tra tỉnh
                UBND Huyện Đông Hưng`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Văn phòng UBND Tỉnh
        Sở Giao thông Vận tải
        Sở Ngoại vụ
        Sở Nội vụ
        Sở Tài chính
        Sở Tư pháp
        Sở Xây dựng
        Thanh tra tỉnh`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
